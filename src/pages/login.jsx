import { Grid } from '@mui/material'
import { Container } from '@mui/system'
import React from 'react'
import LogInContent from '../components/loginContent'

const LogIn = () => {
  return (
    <>
      <Container maxWidth="xxl" className="bgLogin">
        <Grid container>
          <Grid item sm={4} md={4} l={4} lg={4} className="loginContainer">
            <LogInContent/>
          </Grid>
        </Grid>
       
      </Container>
    </>
  )
}

export default LogIn
