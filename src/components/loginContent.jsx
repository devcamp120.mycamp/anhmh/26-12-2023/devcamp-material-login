import { PersonOutline, Visibility, VisibilityOff } from '@mui/icons-material'
import { Box, Typography, FormControl, InputLabel, Input, InputAdornment, IconButton, Button } from '@mui/material'
import React from 'react';

const LogInContent = () => {
  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <>
      <Box className='loginForm'>
        <Typography variant="h2" className="loginTitle" sx={{ m: 3 }}>Login</Typography>
        <FormControl sx={{ m: 3, borderBottom: '1px solid #ffffff' }} variant="standard" fullWidth>
          <InputLabel htmlFor="username" className='loginItem'>
            Username
          </InputLabel>
          <Input
            id="username"
            className='loginItem'
            endAdornment={
              <InputAdornment position="start" sx={{ color: '#ffffff' }}>
                <PersonOutline />
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl sx={{ m: 3, borderBottom: '1px solid #ffffff' }} fullWidth variant="standard" >
          <InputLabel htmlFor="password" className='loginItem'>Password</InputLabel>
          <Input
            id="password"
            className='loginItem'
            type={showPassword ? 'text' : 'password'}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  sx={{ color: "#ffffff" }}
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <Button sx={{ m: 3 }} variant="contained" fullWidth className='loginButton'>Login</Button>
      </Box>
    </>
  )
}

export default LogInContent
